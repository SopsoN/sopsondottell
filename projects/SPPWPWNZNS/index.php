<?php 
	require_once("../../wp-load.php");
?>
<?php get_header('post');	?>

    <link rel="stylesheet" href="TemplateData/style.css">
    <script src="TemplateData/UnityProgress.js"></script>  
    <script src="Build/UnityLoader.js"></script>
    <script>
      var gameInstance = UnityLoader.instantiate("gameContainer", "Build/SPPWPWNZNS.json", {onProgress: UnityProgress});
    </script>

	<div id="content" class="pure-g" style="display: block; text-align: center; padding: 2rem 0;">
		<div class="pure-u-1-1">
			<h2>SKAKANIE PO PLATFORMACH W PRAWO W NIESKOŃCZNOŚĆ ŻEBY NIE SPAŚĆ</h2>
		</div>
	    <div class="webgl-content" style="display: inline-block; text-align: center; margin: 0 auto; position: relative; top: auto; left: auto; -webkit-transform: none; transform: none;">
			<div id="gameContainer" style="width: 960px; height: 600px"></div>
			<div class="footer">
				<div class="webgl-logo"></div>
				<div class="fullscreen" onclick="gameInstance.SetFullscreen(1)"></div>
			</div>
	    </div>
	</div>
	
<?php get_footer();	?>