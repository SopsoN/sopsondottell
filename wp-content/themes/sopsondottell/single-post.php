<?php 
	get_header('post');
	global $post;
		
	$query = new WP_QUERY(array('p' => $post->ID));
		 	
	if( $query->have_posts() ):
		while( $query->have_posts() ):
			$query->the_post();
?>

<div class="post-header-page pure-g" style="background: url(<?php echo get_the_post_thumbnail_url(); ?>)">
	<div class="post-header-page-overlay">
		<div class="pure-u-1">
			<h1><?php echo get_the_title(); ?></h1>
		</div>
	</div>
</div>

<div class="post-content pure-g">
	<?php echo the_content(); ?>
</div>

<?php 
		endwhile;
	else:
?>

<div class="pure-g" style="text-align: center;">
	<div class="pure-u-1-1">
		<h1 class="text-center">ERROR 404 - wpis nie został znaleziony</h1>
		<h3 class="text-center">Chyba zbłądziłeś przyjacielu &#x1F642;</h3>
	</div>
	
	<div class="pure-u-1-1">
		<img src="<?php echo get_template_directory_uri().'/assets/img/404_misiek.png'; ?>" alt="Nie znaleziono wpisu" style="max-width: 100%;"/>
	</div>
</div>

<?php
	endif;
	
	get_footer(); 
?>