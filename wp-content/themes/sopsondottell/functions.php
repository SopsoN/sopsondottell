<?php
	
add_theme_support( 'post-thumbnails', array( 'post', 'page' ) );

function include_css()
{
    wp_enqueue_style( 'style', get_stylesheet_uri() );
    wp_enqueue_style( 'pure-min', get_template_directory_uri().'/assets/css/pure/pure-min.css' );
    wp_enqueue_style( 'base-min', get_template_directory_uri().'/assets/css/pure/base-min.css' );
    wp_enqueue_style( 'grids-min', get_template_directory_uri().'/assets/css/pure/grids-min.css' );
    wp_enqueue_style( 'grids-responsive-min', get_template_directory_uri().'/assets/css/pure/grids-responsive-min.css' );
    wp_enqueue_style( 'remodal', get_template_directory_uri().'/assets/css/remodal.css' );
    wp_enqueue_style( 'remodal-default-theme', get_template_directory_uri().'/assets/css/remodal-default-theme.css' );
}
add_action( 'wp_enqueue_scripts', 'include_css' );

function include_js()
{
    wp_enqueue_script( 'remodal.min', get_template_directory_uri().'/assets/js/remodal.min.js', array( 'jquery' ) );
    wp_enqueue_script( 'sopsondottell', get_template_directory_uri().'/assets/js/sopsondottell.js' );
}
add_action( 'wp_enqueue_scripts', 'include_js' );