<!DOCTYPE php>
<html lang="pl-PL">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="author" content="Michał 'SopsoN' Sobczak" />
	<meta name="description" content="Portfolio developera złożona z projektów po godzina pracy. Michał 'SopsoN' Sobczak" />
	<meta name="keywords" content="programista, developer, android, html, css, jquery, sklep, www, php, strony internetowe, strony www, strony, internet, woocommerce, wordpress, informatyk, online, student, tanio, twórca, sopson, sopek, michał sobczak" />
	
    <title>Michał 'SopsoN' Sobczak</title>
	<link href="https://fonts.googleapis.com/css?family=Alegreya|Amatic+SC|Bellefair|Lato|Oswald" rel="stylesheet">
	<script src="https://use.fontawesome.com/9d7e23d145.js"></script>
	<?php wp_head(); ?>
</head>
<body>
	
<div id="mySidenav" class="sidenav">
	<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
	 
	<div class="pure-g">
		<div class="pure-u-1-1">
			<img src="<?php echo get_template_directory_uri().'/assets/img/profil.jpg'; ?>" alt="Michał Sobczak" />
		</div>
		<div id="bio-personal-details" class="pure-u-1-1">
			<ul>
				<li><strong>Imię:</strong> <span>Michał</span></li>
				<li><strong>Nazwisko:</strong> <span>Sobczak</span></li>
				<?php
		            $birth 	= new DateTime("1996-12-02");
		            $now 	= new DateTime("now");
		            $diff 	= $now->diff($birth); 
		        ?>
				<li><strong>Wiek:</strong> <span><?php echo $diff->format("%Y"); ?></span></li>
			</ul>
		</div>	
		<div class="pure-u-1-1">
			<h3>O mnie</h3>
			<div class="bio-smiple-bottom-border"></div>
			<p>Lubie grać w koszykówkę, siatkówkę. Lubię również programować, dlatego tworzenie stron, aplikacji mobilnych czy aplikacji desktopowych to dla mnie czysta przyjemność.</p>
		</div>	
		<div class="pure-u-1-1">
			<h3>Doświadczenie zawodowe</h3>
			<div class="bio-smiple-bottom-border"></div>
			<ol>
				<li>Pixelmeal s.c. - Administrator stron WWW, czerwiec 2016 - obecnie</li>
				<li>Coderia sp. z o.o. - Młodszy programista C#, lipiec - sierpień 2015</li>
				<li>Coderia sp. z o.o. - Praktyka zawodowa, maj 2015</li>
				<li>Prowadzenie <a href="http://www.nto.pl/wiadomosci/olesno/art/4643359,uczniowie-z-olesna-prowadza-dla-seniorow-darmowy-kurs-komputerowy,id,t.html" target="_blank">kursu z obsługi komputera dla seniorów</a>, listopad 2014 - kwiecień 2015</li>
			</ol>
		</div>
		<div class="pure-u-1-1">
			<h3>Wykształcenie</h3>
			<div class="bio-smiple-bottom-border"></div>
			<ul>
				<li>Informatyka na Politechnice Opolskiej  - obecnie</li>
				<li>Technikum Informatyczne</li>
			</ul>
		</div>
		
		<div class="pure-u-1-1">
			<h3>Kwalifikacje/Certyfikaty</h3>
			<div class="bio-smiple-bottom-border"></div>
			<ul>
				<li>Kwalifikacja E14 Tworzenie aplikacji internetowych i baz danych oraz administrowanie bazami.</li>
				<li>Kwalifikacja E13 Projektowanie lokalnych sieci komputerowych i administrowanie sieciami.</li>
				<li>Kwalifikacja E12 Montaż i eksploatacja komputerów osobistych oraz urządzeń peryferyjnych.</li>
				<li>Internetowe Rewolucje - Google.</li>
			</ul>
		</div>
	</div>
</div>
	
<div id="wrapper">
<div id="wrapper-remodal-bg" class="remodal-bg">
<div id="nav-menu" class="pure-g">
	<div id="my-bio" class="pure-u-1-3">
		<a href="#" onclick="openNav()">
			<i class="fa fa-user-circle-o" aria-hidden="true"></i>
			<span>BIO</span>
		</a>
	</div>
	<div class="pure-u-1-3" onclick="closeNav()">
		<a href="<?php echo home_url();?>"><img class="pure-img" src="<?php echo get_template_directory_uri().'/assets/img/sopson_logo.png' ?>" alt="Logo Michał 'SopsoN' Sobczak"/></a>
	</div>
	<div class="pure-u-1-3" onclick="closeNav()"></div>
	<div class="pure-u-1-3" onclick="closeNav()"></div>
	<div class="pure-u-1-3" onclick="closeNav()">
		<a href="<?php echo home_url();?>" style="color:#FFF; text-decoration: none;"><p>MICHAŁ <span>'SOPSON'</span> SOBCZAK</p></a>
	</div>
	<div class="pure-u-1-3" onclick="closeNav()"></div>	
</div>
