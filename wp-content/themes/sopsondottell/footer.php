<div id="footer-copy" class="pure-g">
	<div class="pure-u-sm-1-2 pure-u-1">
		<p>Copyrights &copy; <?php echo date("Y"); ?> SopsoN. All rights reserved.</p>
	</div>
	<div class="pure-u-sm-1-2 pure-u-1">
		<p><a href="mailto: michal.sobczak96@gmail.com">michal.sobczak96@gmail.com</a></p>
	</div>
</div>

<?php wp_footer(); ?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-105739783-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-105739783-1');
</script>

</body>
</html>