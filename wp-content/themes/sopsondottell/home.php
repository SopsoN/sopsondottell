<?php 
	get_header();
?>
	<div id="content" class="pure-g" onclick="closeNav()">
		<?php 
			for( $i = 0; $i < count($posts); $i++)	
			{
				$class = "";
				
				if( $i < 2 )
				{
					$class = "pure-u-sm-1-2 pure-u-1";
				}
				else if( $i >= 2 && $i <= 4) // $i >= 2 && $i < 4	
				{
					//$class = "pure-u-sm-1-2 pure-u-1";
					$class = "pure-u-sm-1-3 pure-u-1";
				}
				else
				{
					$class = "pure-u-sm-1-6 pure-u-1";
				}
						
				echo "<div class=\"post-container ".$class."\" style=\"background-image: url(".$posts[$i]['thumbnail']." )\">";
					echo "<div class=\"post-holder\">";
						echo "<h3>".$posts[$i]['title']."</h3>";
						echo "<p>".$posts[$i]['exerpt']."</p>";
						echo "<a href=\"#".$posts[$i]['title']."\">Czytaj dalej</a>";
					echo "</div>";
				echo "</div>";
			}
		?>
	</div>
	
	<div id="remodal-content" class="pure-g">
		<?php
			for( $i = 0; $i < count($posts); $i++)	
			{
				echo "<div class=\"remodal\" data-remodal-id=\"".$posts[$i]['title']."\">";
					echo "<div class=\"remodal-header\">";
						echo "<button data-remodal-action=\"close\" class=\"remodal-close\"></button>";
						echo "<h1>".$posts[$i]['title']."</h1>";
					echo "</div>";
					
					echo '<div class="pure-g">';
						echo $posts[$i]['text'];
					echo '</div>';
					
					echo "<div class=\"footer-remodal\">";
						echo "<p>";
							echo '<span style="text-align:left;">';
								echo '<a href="'.$posts[$i]['permalink'].'" alt="Otwórz wpis jako stronę w osobnej karcie" target="_blank">';
									echo '<i class="fa fa-external-link" aria-hidden="true"></i>';
								echo '</a>';
							echo '</span>';
							echo "<span>".$posts[$i]['date']."r.</span>";
						echo "</p>";
					echo "</div>";
				echo "</div>";
			}
		?>
	</div>
	</div>
</div>
<?php
	get_footer();