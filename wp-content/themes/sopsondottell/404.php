<?php get_header('post'); ?>

<div class="pure-g" style="text-align: center;">
	<div class="pure-u-1-1">
		<h1 class="text-center">ERROR 404 - wpis nie został znaleziony</h1>
		<h3 class="text-center">Chyba zbłądziłeś przyjacielu &#x1F642;</h3>
	</div>
	
	<div class="pure-u-1-1">
		<img src="<?php echo get_template_directory_uri().'/assets/img/404_misiek.png'; ?>" alt="Nie znaleziono wpisu" style="max-width: 100%;" />
	</div>
</div>

<?php get_footer(); ?>